package com.mybatisplus.kc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mybatisplus.kc.model.MenuEntity;
import com.mybatisplus.kc.model.Permission;
import com.mybatisplus.kc.model.Role;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Set;

/**
 * author shish
 * Create Time 2019/3/7 16:47
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface PermissionMapper extends BaseMapper<MenuEntity> {

    Set<Permission> findPermissionsByRoleId(@Param("roles") Set<Role> roles);
}
