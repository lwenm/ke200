package com.mybatisplus.kc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mybatisplus.kc.model.User;

/**
 * author shish
 * Create Time 2019/3/7 16:45
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface UserMapper extends BaseMapper<User> {
}
