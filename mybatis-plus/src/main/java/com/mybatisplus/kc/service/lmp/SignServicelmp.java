package com.mybatisplus.kc.service.lmp;

import com.mybatisplus.kc.mapper.SignMapper;
import com.mybatisplus.kc.model.SignEntity;
import com.mybatisplus.kc.model.StudentEntity;
import com.mybatisplus.kc.service.SignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author shish
 * Create Time 2019/4/29 16:33
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class SignServicelmp  implements SignService {

  @Autowired
    private SignMapper signMapper;

    @Override
    public Integer nomal(String class_grade, String start, String end) {
        return signMapper.normal(class_grade,start,end);
    }

    @Override
    public List<SignEntity> unNotmal(String class_grade, String start, String end) {
        return signMapper.unNormal(class_grade,start,end);
    }

    @Override
    public List<SignEntity> normalList(String class_grade, String start, String end) {
        return signMapper.NormalList(class_grade,start,end);
    }

    @Override
    public List<SignEntity> unNormalList(String class_grade, String start, String end) {
        return signMapper.unNormalList(class_grade,start,end);
    }

    @Override
    public List<SignEntity> match(String class_grade, Integer course_id) {
        return signMapper.match(class_grade,course_id);
    }
}
