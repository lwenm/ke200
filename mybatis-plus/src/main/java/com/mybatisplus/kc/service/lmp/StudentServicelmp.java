package com.mybatisplus.kc.service.lmp;

import com.mybatisplus.kc.mapper.StudentMapper;
import com.mybatisplus.kc.model.StudentEntity;
import com.mybatisplus.kc.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author shish
 * Create Time 2019/4/29 11:09
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class StudentServicelmp implements StudentService {
    @Autowired
    private StudentMapper studentMapper;
    @Override
    public List<StudentEntity> selectList(String class_grade,Integer uid) {
        return studentMapper.queryList(class_grade,uid);
    }

    @Override
    public Integer save(StudentEntity studentEntity) {
        return studentMapper.insert(studentEntity);
    }

    @Override
    public StudentEntity selectOne(Long userId) {
        return studentMapper.selectById(userId);
    }
}
