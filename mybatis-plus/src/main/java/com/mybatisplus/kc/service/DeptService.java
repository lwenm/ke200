package com.mybatisplus.kc.service;

import com.mybatisplus.kc.model.DeptEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * author shish
 * Create Time 2019/3/8 15:24
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface DeptService {
  int addDept(DeptEntity deptEntity);
  int delDept(ArrayList ids);
  int delDept(Integer id);
  int update(DeptEntity deptEntity);
  DeptEntity selectOne(Integer id);
  List<DeptEntity> selectDeptList(Integer uid);


}
