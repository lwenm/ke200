package com.mybatisplus.kc.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * author shish
 * Create Time 2019/1/13 19:35
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@TableName("sys_role")
public class Role implements Serializable {
    /**
     * 角色id
     */
    @TableId
    private Integer id;

    /**
     * 角色标识程序中判断使用,如"admin"
     */
    @TableId
    private String role;

    /**
     * 角色描述,UI界面显示使用
     */
    @TableId
    private String description;

    /**
     * 是否可用0可用  1不可用
     */
    @TableId
    private String available;

    /**
     * 某个角色对应的所有用户
     */
    @TableField(exist = false)
    private Set<User> users = new HashSet<>();

    /**
     * 某个角色对应的所有权限
     */
    @TableField(exist = false)
    private Set<Permission> permissions = new HashSet<>();

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", role='" + role + '\'' +
                ", description='" + description + '\'' +
                ", available='" + available + '\'' +
                ", users=" + users +
                ", permissions=" + permissions +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }
}
