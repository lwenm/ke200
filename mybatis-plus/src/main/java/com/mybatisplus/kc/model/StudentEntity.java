package com.mybatisplus.kc.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * author shish
 * Create Time 2019/4/29 11:11
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@TableName("ke_user")
public class StudentEntity implements Serializable {
    @TableId(value = "user_id")
    private Long userId;//学号
    @TableField
    private String password;//登陆密码
    @TableField
    private  String name;//名字
    @TableField
    private String grade;//年级
    @TableField
    private  Integer sex;//性别
    @TableField
    private  String nickname;//昵称
    @TableField
    private  String per_sign;//
    @TableField
    private  String email;//电子邮箱
    @TableField
    private String url;//头像地址
    @TableField
    private String question;//问题
    @TableField
    private String answer;//答案
    @TableField
    private String tel;//电话
    @TableField
    private  String class_grade;

    public String getClass_grade() {
        return class_grade;
    }

    public void setClass_grade(String class_grade) {
        this.class_grade = class_grade;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPer_sign() {
        return per_sign;
    }

    public void setPer_sign(String per_sign) {
        this.per_sign = per_sign;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
