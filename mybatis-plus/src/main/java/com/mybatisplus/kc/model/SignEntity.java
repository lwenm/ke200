package com.mybatisplus.kc.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * author shish
 * Create Time 2019/4/29 16:22
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@TableName("kc_sign")
public class SignEntity {
    @TableId(value = "id",type = IdType.AUTO)
    private  Integer id;
     @TableField
    private Integer kc_class_time_id;
     @TableField
     private  Integer stu_id;
     @TableField
     @JSONField(format = "yyyy-MM-dd HH:mm:ss")  //FastJson包使用注解
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") //Jackson包使用注解
     @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")   //格式化前台日期参数注解
     private Date sign_time;
     @TableField
     private  Integer sign_status;

     private String name;
     //课程id
     private Integer course_id;
     //经度
     private  String longtitude;
     //维度
     private  String lantitude;
    //年级信息
     private  String class_grade;

     private  Integer kc_stu_id;
     //签到的次数
     private  Integer num;

     private  String course_name;

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getKc_stu_id() {
        return kc_stu_id;
    }

    public void setKc_stu_id(Integer kc_stu_id) {
        this.kc_stu_id = kc_stu_id;
    }

    public Integer getCourse_id() {
        return course_id;
    }

    public void setCourse_id(Integer course_id) {
        this.course_id = course_id;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getLantitude() {
        return lantitude;
    }

    public void setLantitude(String lantitude) {
        this.lantitude = lantitude;
    }

    public String getClass_grade() {
        return class_grade;
    }

    public void setClass_grade(String class_grade) {
        this.class_grade = class_grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKc_class_time_id() {
        return kc_class_time_id;
    }

    public void setKc_class_time_id(Integer kc_class_time_id) {
        this.kc_class_time_id = kc_class_time_id;
    }

    public Integer getStu_id() {
        return stu_id;
    }

    public void setStu_id(Integer stu_id) {
        this.stu_id = stu_id;
    }

    public Date getSign_time() {
        return sign_time;
    }

    public void setSign_time(Date sign_time) {
        this.sign_time = sign_time;
    }

    public Integer getSign_status() {
        return sign_status;
    }

    public void setSign_status(Integer sign_status) {
        this.sign_status = sign_status;
    }
}
