package com.mybatisplus.kc.abstractController;

import com.mybatisplus.kc.model.User;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * author shish
 * Create Time 2019/3/7 17:24
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public abstract class AbstractController {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected User getUser() {
        return (User) SecurityUtils.getSubject().getPrincipal();
    }
    protected Integer getUserId() {
        logger.info("当前登陆系统的用户"+String.valueOf(getUser().getUid()));
        return getUser().getUid();
    }
}
