var vm = new Vue({
    el: '#app',
    data: {
        start: "",
        end: "",
        course_id: "",
        course: "",
        search: "",
        columns4:[
            {
                title:"学号",
                key: 'stu_id',
            },
            {
                title:"名字",
                key: 'name',
            },
            {
                title:"班级",
                key: 'class_grade',
            },
            {
                title:"签到时间",
                key: 'sign_time',
            },
            {
                title:"状态",
                key: 'sign_status',
                render:function (h, params) {
                    const row = params.row;
                    const  status=row.sign_status
                    console.log(row.sign_status)
                    if (status==1){
                        return h('div',"正常签到"
                        );/*这里的this.row能够获取当前行的数据*/
                    }
                    else {
                        return h('div', [
                            h('span', {
                                style:{
                                    fontSize: '14px',
                                    cursor: 'pointer',
                                    color: '#ff3046'
                                }
                            }, '未正常签到'),
                        ]);/*这里的this.row能够获取当前行的数据*/
                    }
                }
            }
        ],
        listData:[]

    },
    mounted: function () {
        this.getCourseList()
    },
    methods: {

        show: function () {
            this.visible = true;
        },
        add: function () {

        },
        update: function () {
        },
        del: function () {

        },
        searchBtn: function () {
            console.log(vm.search)
            console.log(vm.course_id)
            axios({
                url: "../sign/couse_total?"+"class_grade="+vm.search+"&course_id="+vm.course_id,
            }).then(function(res){
                console.log("获取签到的课程")
            vm.listData=res.data.list
            console.log(res)
        })
        },
        getCourseList: function () {
            axios({
                url: "../teacher/select_list",
            }).then(function(res){
                console.log("获取签到的课程")
            vm.course = res.data.list
            console.log(res)
        })
        },
        excel:function(){
            console.log("导出表格")
            console.log( vm.listData)
            var len=vm.listData.length
            if (len!=0){
                axios({
                    url: "../sign/export?"+"class_grade="+vm.search+"&course_id="+vm.course_id,
                }).then(function(res){
                    var a = document.createElement('a')
                    a.download = "考情信息表" || "考情信息表"
                    // 地址
                      a.href = "../static/考情信息表.xls";
                     a.click()
            })
            }else {
                alert("没有数据，生成不了excek")
            }

        },
        change_status:function (value) {
            console.log(value)
        }
    }
})