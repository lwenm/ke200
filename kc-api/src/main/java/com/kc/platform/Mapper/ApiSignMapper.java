package com.kc.platform.Mapper;

import com.kc.platform.model.SignEntity;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * author shish
 * Create Time 2019/5/3 12:54
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface ApiSignMapper {
    Integer signSave(SignEntity signEntity);

    SignEntity querySignList(@Param("stu_id") Integer stu_id, @Param("course_id") Integer course_id, @Param("queryDate") String queryDate,@Param("queryDateEnd") String queryDateEnd);

    List<SignEntity> querylistByUserId(@Param("userId") Long userId);
    List<SignEntity> querySignlistByUserId(@Param("userId") Long userId);
}
