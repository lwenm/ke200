package com.kc.platform.apiController;

import com.kc.platform.annotation.IgnoreAuth;
import com.kc.platform.annotation.LoginUser;
import com.kc.platform.common.R;
import com.kc.platform.model.TokenEntity;
import com.kc.platform.model.User;
import com.kc.platform.service.ApiUserService;
import com.kc.platform.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/5/2 21:43
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("apiuser")
public class ApiGetLoginInfoController {
  @Autowired
    private TokenService tokenService;
  @Autowired
  private ApiUserService apiUserService;

    @RequestMapping("/info")
    public R getUserInfo(@LoginUser User user){
        Map<String,Object> result=new HashMap<>();
        result.put("user",user);
        return  R.ok(result);
    }
    @RequestMapping("/login")
    public R getUserInfologin(@LoginUser User user){
        Map<String,Object> result=new HashMap<>();
        result.put("user",user);
        return  R.ok(result);
    }
    @RequestMapping("/token")
    @IgnoreAuth
    public  R getToken(String token){
        TokenEntity tokenEntity=tokenService.queryByToken(token);
        Map<String,Object> result=new HashMap<>();
        result.put("token",tokenEntity);
        return  R.ok(result);
    }
    @RequestMapping("/regist")
    @IgnoreAuth
    public  R regist(User user){
        Map<String,Object> result=new HashMap<>();
        Integer code= apiUserService.save(user);
        try {

            result.put("code",code);
        }catch (Exception e){
            code=1000;
            result.put("code",code);
            e.printStackTrace();
        }
        return R.ok(result);
    };
    @RequestMapping("saveopenid")
    @IgnoreAuth
    public  R saveOpenId(@LoginUser User user,String code){
        return null;
    }
}
