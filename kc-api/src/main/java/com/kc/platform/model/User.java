package com.kc.platform.model;


import java.io.Serializable;

/**
 * author shish
 * Create Time 2019/1/11 15:31
 * author email shisheng@live.com
 * website www.bangnila.com
 **/

public class User implements Serializable {

    private Long userId;//学号
    private String password;//登陆密码
    private  String name;//名字
    //微信openid
    private  String openid;
    private String grade;//年级
    //学院-班级-年级151121
    private String class_grade;
    private  Integer sex;//性别
    private  String nickname;//昵称
    private  String per_sign;//个性签名
    private  String email;//电子邮箱
    private String url;//头像地址
    private String question;//问题
    private String answer;//答案
    private String tel;//电话

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getClass_grade() {
        return class_grade;
    }

    public void setClass_grade(String class_grade) {
        this.class_grade = class_grade;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPer_sign() {
        return per_sign;
    }

    public void setPer_sign(String per_sign) {
        this.per_sign = per_sign;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
