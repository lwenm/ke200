package com.kc.platform.service;

import com.kc.platform.model.TokenEntity;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * author shish
 * Create Time 2019/1/11 16:16
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface TokenService {
    TokenEntity queryByToken(@Param("token") String token);

    Map<String, Object> createToken(long userId);
}
