package com.kc.platform.service;

import com.kc.platform.model.ClassTimeEntity;
import com.kc.platform.model.SignEntity;
import com.kc.platform.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * author shish
 * Create Time 2019/1/11 16:14
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface ApiUserService {
    User queryObject(@Param("userId") Long userId);

    long login(Long number, String password);

    List<ClassTimeEntity> QuerySignList(String grade, String time,String endtime);

    Integer save(User user);

    User getUserByOpenId(String openid);
}
