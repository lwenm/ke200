package com.kc.admin.controller;

import com.kc.admin.model.User;
import com.kc.admin.utils.ShiroUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * author shish
 * Create Time 2019/3/7 11:27
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public abstract class AbstractController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected User getUser() {
        return (User) SecurityUtils.getSubject().getPrincipal();
    }
    protected Integer getUserId() {
        return getUser().getUid();
    }


}
