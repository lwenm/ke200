package com.kc.admin.service.lmp;

import com.kc.admin.mapper.MenuMapper;
import com.kc.admin.mapper.UserMapper;
import com.kc.admin.model.MenuEntity;
import com.kc.admin.model.User;
import com.kc.admin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author shish
 * Create Time 2019/1/12 16:35
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class UserServicelmp implements UserService {
  @Autowired
  private UserMapper userMapper;
  @Autowired
  private MenuMapper menuMapper;
    @Override
    public int insertUserInfo(User userInfo) {
        return userMapper.insertUserInfo(userInfo);
    }

    @Override
    public User findByUserName(String userName) {
        return userMapper.findByUserName(userName);
    }

    @Override
    public List<MenuEntity> getMenuByUserId(Integer userId) {
        return menuMapper.getMenuByUserId(userId);
    }
}
